<?php
$finder = Symfony\CS\Finder\DefaultFinder::create()
//    ->notPath('Zend/View/Stream.php')
//    ->notPath('ZendTest/Code/Generator/TestAsset')
//    ->notPath('ZendTest/Code/Reflection/FunctionReflectionTest.php')
//    ->notPath('ZendTest/Code/Reflection/MethodReflectionTest.php')
//    ->notPath('ZendTest/Code/Reflection/TestAsset')
//    ->notPath('ZendTest/Code/TestAsset')
//    ->notPath('ZendTest/Validator/_files')
//    ->notPath('ZendTest/Loader/_files')
//    ->notPath('ZendTest/Loader/TestAsset')
//    ->notPath('demos')
//    ->notPath('resources')
//    ->filter(function (SplFileInfo $file) {
//        if (strstr($file->getPath(), 'compatibility')) {
//            return false;
//        }
//    })
//    ->in(__DIR__ . '/bin')
    ->in(__DIR__ . '/src')
    ->in(__DIR__ . '/tests');
$config = Symfony\CS\Config\Config::create();
$config->fixers(
    [
        'encoding', // [PSR-1] PHP code MUST use only UTF-8 without BOM (remove BOM).
        'linefeed', // [PSR-2] All PHP files must use the Unix LF (linefeed) line ending.
        'indentation', // [PSR-2] Code must use 4 spaces for indenting, not tabs.
        'trailing_spaces', // [PSR-2] Remove trailing whitespace at the end of lines.
        'unused_use', // [all] Unused use statements must be removed.
        // 'object_operator', // [all] There should not be space before or after object T_OBJECT_OPERATOR.
        'phpdoc_params', // [all] All items of the @param phpdoc tags must be aligned vertically.
        'visibility', // [PSR-2] Visibility must be declared on all properties and methods; abstract and final must be declared before the visibility; static must be declared after the visibility.
        'short_tag', // [PSR-1] PHP code must use the long <?php ? > tags or the short-echo <?= ? > tags; it must not use the other tag variations.
        'php_closing_tag', // [PSR-2] The closing ? > tag MUST be omitted from files containing only PHP.
        // 'return', // [all] An empty line feed should precede a return statement.
        'extra_empty_lines', // [all] Removes extra empty lines.
        'braces', // [PSR-2] Opening braces for classes, interfaces, traits and methods must go on the next line, and closing braces must go on the next line after the body. Opening braces for control structures must go on the same line, and closing braces must go on the next line after the body.
        'lowercase_constants', // [PSR-2] The PHP constants true, false, and null MUST be in lower case.
        'lowercase_keywords', // [PSR-2] PHP keywords MUST be in lower case.
        'include', // [all] Include and file path should be divided with a single space. File path should not be placed under brackets.
        'function_declaration', // [PSR-2] Spaces should be properly placed in a function declaration
        // 'controls_spaces', // [all] A single space should be between: the closing brace and the control, the control and the opening parentheses, the closing parentheses and the opening brace.
        // 'spaces_cast', // [all] A single space should be between cast and variable.
        'psr0', // [PSR-0] Classes must be in a path that matches their namespace, be at least one namespace deep, and the class name should match the file name.
        'elseif', // [PSR-2] The keyword elseif should be used instead of else if so that all control keywords looks like single words.
        'eof_ending', // [PSR-2] A file must always end with an empty line feed.
        'standardize_not_equal', // [all] Replace all <> with !=.
        'new_with_braces', // [all] All instances created with new keyword must be followed by braces.
    ]
);
$config->finder($finder);
return $config;

