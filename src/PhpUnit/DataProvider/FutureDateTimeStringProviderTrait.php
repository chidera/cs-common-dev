<?php
namespace CsDev\PhpUnit\DataProvider;

use DateTime;

/**
 * Provides a simple set of future DateTime string values.
 */
trait FutureDateTimeStringProviderTrait
{
    public function futureDateTimeStringProvider()
    {
        return [
            ['tomorrow'],
            ['next second'],
            ['next minute'],
            ['next hour'],
            ['next day'],
            ['next fortnight'],
            ['next month'],
            ['next year'],
            ['next sunday'],
            ['next monday'],
            ['next tuesday'],
            ['next wednesday'],
            ['next thursday'],
            ['next friday'],
            ['next saturday'],
        ];
    }
}
