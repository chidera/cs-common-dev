<?php
namespace CsDev\PhpUnit\DataProvider;

/**
 * Provides a simple set of negative integer values.
 */
trait NegativeIntegerProviderTrait
{
    public function negativeIntegerProvider()
    {
        return [
            [-1],
        ];
    }
}
