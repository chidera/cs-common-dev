<?php
namespace CsDev\PhpUnit\DataProvider;

use stdClass;

/**
 * Provides a simple set of non-integer values.
 */
trait NonIntegerProviderTrait
{
    public function nonIntegerProvider()
    {
        return [
            [null],
            [true],
            [false],
            ['123'],
            ['0'],
            ['-123'],
            [''],
            [' '],
            ['some.string'],
            [1.23],
            [-1.23],
            [new stdClass()],
            [[]],
            [['some', 'odd', 'array']],
        ];
    }
}
