<?php
namespace CsDev\PhpUnit\DataProvider;

use stdClass;

/**
 * Provides a simple set of non-DateTime values.
 */
trait NonDateTimeProviderTrait
{
    public function nonDateTimeProvider()
    {
        return [
            [null],
            [true],
            [false],
            ['123'],
            ['0'],
            ['-123'],
            [''],
            [' '],
            ['some.string'],
            [123],
            [0],
            [-123],
            [1.23],
            [-1.23],
            [new stdClass()],
            [[]],
            [['some', 'odd', 'array']],
        ];
    }
}
