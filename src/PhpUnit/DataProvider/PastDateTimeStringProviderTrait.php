<?php
namespace CsDev\PhpUnit\DataProvider;

/**
 * Provides a simple set of past DateTime string values.
 */
trait PastDateTimeStringProviderTrait
{
    public function pastDateTimeStringProvider()
    {
        return [
            ['yesterday'],
            ['last second'],
            ['last minute'],
            ['last hour'],
            ['last day'],
            ['last fortnight'],
            ['last month'],
            ['last year'],
            ['last sunday'],
            ['last monday'],
            ['last tuesday'],
            ['last wednesday'],
            ['last thursday'],
            ['last friday'],
            ['last saturday'],
        ];
    }
}
