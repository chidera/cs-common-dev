<?php
namespace CsDev\PhpUnit\DataProvider;

/**
 * Provides a simple set of non-positive integer values.
 */
trait NonPositiveIntegerProviderTrait
{
    public function nonPositiveIntegerProvider()
    {
        return [
            [0],
            [-1],
        ];
    }
}
