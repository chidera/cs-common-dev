<?php
namespace CsDevTest\Unit\PhpUnit\DataProvider;

use CsDev\PhpUnit\DataProvider\PastDateTimeStringProviderTrait;
use DateTime;

class PastDateTimeStringProviderTraitTest extends \PHPUnit_Framework_TestCase
{
    public function traitDataProvider()
    {
        return ($this->getObjectForTrait(PastDateTimeStringProviderTrait::class))->pastDateTimeStringProvider();
    }

    /**
     * Each of the data provider's test values is a string.
     *
     * @test
     * @dataProvider traitDataProvider
     */
    public function testValuesAreStrings($testValue)
    {
        $this->assertInternalType('string', $testValue);
    }

    /**
     * Each of the data provider's test values yields a valid DateTime object.
     *
     * @test
     * @depends testValuesAreStrings
     * @dataProvider traitDataProvider
     */
    public function testValuesAreValid($testValue)
    {
        $this->assertInstanceOf(DateTime::class, new DateTime($testValue));
    }

    /**
     * Each of the data provider's test values yields a date/time in the past.
     *
     * @test
     * @depends testValuesAreValid
     * @dataProvider traitDataProvider
     */
    public function testValuesAreInTheFuture($testValue)
    {
        $now = new DateTime();
        $future = (clone $now)->modify($testValue);

        $this->assertLessThan($now, $future);
    }
}
