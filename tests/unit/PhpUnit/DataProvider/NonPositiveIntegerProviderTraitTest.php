<?php
namespace CsDevTest\Unit\PhpUnit\DataProvider;

use CsDev\PhpUnit\DataProvider\NonPositiveIntegerProviderTrait;

class NonPositiveIntegerProviderTraitTest extends \PHPUnit_Framework_TestCase
{
    public function traitDataProvider()
    {
        return ($this->getObjectForTrait(NonPositiveIntegerProviderTrait::class))->nonPositiveIntegerProvider();
    }

    /**
     * Each of the data provider's test values is an integer.
     *
     * @test
     * @dataProvider traitDataProvider
     */
    public function testValuesAreIntegers($testValue)
    {
        $this->assertInternalType('integer', $testValue);
    }

    /**
     * Each of the data provider's test values is non-positive.
     *
     * @test
     * @depends testValuesAreIntegers
     * @dataProvider traitDataProvider
     */
    public function testValuesAreNonPositive($testValue)
    {
        $this->assertLessThan(1, $testValue);
    }
}
