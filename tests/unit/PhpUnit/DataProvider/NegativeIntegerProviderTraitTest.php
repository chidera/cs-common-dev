<?php
namespace CsDevTest\Unit\PhpUnit\DataProvider;

use CsDev\PhpUnit\DataProvider\NegativeIntegerProviderTrait;

class NegativeIntegerProviderTraitTest extends \PHPUnit_Framework_TestCase
{
    public function traitDataProvider()
    {
        return ($this->getObjectForTrait(NegativeIntegerProviderTrait::class))->negativeIntegerProvider();
    }

    /**
     * Each of the data provider's test values is an integer.
     *
     * @test
     * @dataProvider traitDataProvider
     */
    public function testValuesAreIntegers($testValue)
    {
        $this->assertInternalType('integer', $testValue);
    }

    /**
     * Each of the data provider's test values is negative.
     *
     * @test
     * @depends testValuesAreIntegers
     * @dataProvider traitDataProvider
     */
    public function testValuesAreNegative($testValue)
    {
        $this->assertLessThan(0, $testValue);
    }
}
