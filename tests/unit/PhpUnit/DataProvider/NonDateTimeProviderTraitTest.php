<?php
namespace CsDevTest\Unit\PhpUnit\DataProvider;

use CsDev\PhpUnit\DataProvider\NonDateTimeProviderTrait;
use DateTime;

class NonDateTimeProviderTraitTest extends \PHPUnit_Framework_TestCase
{
    public function traitDataProvider()
    {
        return ($this->getObjectForTrait(NonDateTimeProviderTrait::class))->nonDateTimeProvider();
    }

    /**
     * Each of the data provider's test values is *not* a DateTime object.
     *
     * @test
     * @dataProvider traitDataProvider
     */
    public function testValuesAreNotDateTimeObjects($testValue)
    {
        $this->assertNotInstanceOf(DateTime::class, $testValue);
    }
}
