<?php
namespace CsDevTest\Unit\PhpUnit\DataProvider;

use CsDev\PhpUnit\DataProvider\NonIntegerProviderTrait;

class NonIntegerProviderTraitTest extends \PHPUnit_Framework_TestCase
{
    public function traitDataProvider()
    {
        return ($this->getObjectForTrait(NonIntegerProviderTrait::class))->nonIntegerProvider();
    }

    /**
     * Each of the data provider's test values is *not* an integer.
     *
     * @test
     * @dataProvider traitDataProvider
     */
    public function testValuesAreNotIntegers($testValue)
    {
        $this->assertNotInternalType('integer', $testValue);
    }
}
